package cn.ydxiaoshuai.modules.controller;

import cn.ydxiaoshuai.common.api.vo.api.GarbageBean;
import cn.ydxiaoshuai.common.api.vo.api.GarbageResponseBean;
import cn.ydxiaoshuai.common.constant.JDAIConts;
import cn.ydxiaoshuai.common.system.base.controller.ApiRestController;
import cn.ydxiaoshuai.common.util.api.GarbageUtil;
import cn.ydxiaoshuai.modules.conts.LogTypeConts;
import cn.ydxiaoshuai.modules.util.ApiBeanUtil;
import cn.ydxiaoshuai.modules.weixin.po.WXAccessToken;
import com.alibaba.fastjson.JSON;
import com.baidu.aip.util.Base64Util;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;


/**
 * @author 小帅丶
 * @className GarbageRestController
 * @Description 垃圾分类
 * @Date 2020/4/26-16:24
 **/
@Controller
@RequestMapping(value = "/rest/garbage")
@Scope("prototype")
@Slf4j
@Api(tags = "垃圾分类-API")
public class GarbageRestController extends ApiRestController {
    @Autowired
    private ApiBeanUtil apiBeanUtil;
    /**
     * @Description 垃圾分类查询接口
     * @param file 图片文件
     * @return void
     * @Author 小帅丶
     * @Date 2020年4月26日
     **/
    @RequestMapping(value = "/search", method = {RequestMethod.POST}, produces="application/json;charset=UTF-8")
    public ResponseEntity<Object> detectAcnespotmole(@RequestParam(value = "file") MultipartFile file) {
        log.info("方法路径{}", requestURI);
        //城市ID
        String cityId = ServletRequestUtils.getStringParameter(request, "cityId","310000");
        GarbageBean bean = new GarbageBean();
        GarbageResponseBean garbageResponseBean = new GarbageResponseBean();
        try {
            startTime = System.currentTimeMillis();
            String imgBase64 = Base64Util.encode(file.getBytes());
            param = "cityId="+cityId+",userId="+userId + ",image="+imgBase64+",version="+version;
            garbageResponseBean = GarbageUtil.getGarbageBean(cityId,imgBase64);
            if(LogTypeConts.API_VERSION.equals(version)){
                WXAccessToken imgCheckBean = apiBeanUtil.checkImg(request, file);
                if(imgCheckBean.getErrcode()==0){
                    if(JDAIConts.CODE_10000.equals(garbageResponseBean.getCode())&&garbageResponseBean.getResult().getStatus()==0){
                        bean = getGarbageData(garbageResponseBean);
                    }else{
                        bean.fail("api error", "API错误", garbageResponseBean.getResult().getStatus());
                    }
                }else{
                    bean.fail("img fail", imgCheckBean.getErrmsg(),imgCheckBean.getErrcode());
                }
            }else{
                if(JDAIConts.CODE_10000.equals(garbageResponseBean.getCode())&&garbageResponseBean.getResult().getStatus()==0){
                    bean = getGarbageData(garbageResponseBean);
                }else{
                    bean.fail("api error", "API错误", garbageResponseBean.getResult().getStatus());
                }
            }
        } catch (Exception e) {
            errorMsg = e.getMessage();
            log.info("垃圾分类查询接口出错了" + errorMsg);
            bean.error("system error", "系统错误");
        }
        //耗时
        timeConsuming = String.valueOf(System.currentTimeMillis() - startTime);
        log.info("耗时{},接口返回内容",timeConsuming, JSON.toJSONString(garbageResponseBean));
        beanStr = JSON.toJSONString(bean);
        apiBeanUtil.putLog(bean.getLog_id(), timeConsuming, beanStr, ip, param, requestURI, errorMsg, LogTypeConts.GARBAGE_SEARCH,userId,userAgent);
        //响应的内容
        return new ResponseEntity<Object>(JSON.toJSONString(bean), httpHeaders, HttpStatus.OK);
    }

    /**
     * @Author 小帅丶
     * @Description 处理对象
     * @Date  2020/4/26 17:18
     * @param garbageResponseBean 垃圾查询分类对象
     * @return cn.ydxiaoshuai.tools.vo.GarbageBean.Data
     **/
    private GarbageBean getGarbageData(GarbageResponseBean garbageResponseBean) {
        GarbageBean bean = new GarbageBean();
        GarbageBean.Data data = new GarbageBean.Data();
        GarbageResponseBean.ResultBean.GarbageInfoBean garbageInfoBean = garbageResponseBean.getResult().getGarbage_info().get(0);
        if(garbageInfoBean.getConfidence()>0.7){
            data.setGarbage_name(garbageInfoBean.getGarbage_name());
            data.setCate_name(garbageInfoBean.getCate_name());
            data.setCity_id(garbageInfoBean.getCity_id());
            data.setCity_name(garbageInfoBean.getCity_name());
            data.setConfidence(garbageInfoBean.getConfidence());
            data.setPs(garbageInfoBean.getPs());
            bean.success("success", "分类查询成功", data);
        }else{
            bean.fail("low confidence", "置信度过低",14108);
        }
        return bean;
    }
}

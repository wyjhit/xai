package cn.ydxiaoshuai.common.api.vo.config;

import cn.ydxiaoshuai.common.api.vo.BaseResponseBean;
import cn.ydxiaoshuai.common.constant.CommonConstant;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.List;

/**
 * @author 小帅丶
 * @className IsmIndexSlideResult
 * @Description 首页轮播图对象
 * @Date 2020/3/25-11:52
 **/
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class LiteConfigSlideResult extends BaseResponseBean {
    /**
     * 具体参数
     **/
    private List<SlideListData> data;
    @Data
    public static class SlideListData{
        //轮播图名称
        private String slide_name;
        //轮播图URL
        private String slide_img_url;
        //轮播图web跳转地址
        private String slide_web_url;
        //轮播图小程序跳转路由
        private String slide_lite_url;
        //排序ID
        private Integer slide_sort;
    }
    public LiteConfigSlideResult success(String message, List<SlideListData> data) {
        this.message = message;
        this.code = CommonConstant.SC_OK_200;
        this.data = data;
        return this;
    }
    public LiteConfigSlideResult fail(String message, Integer code) {
        this.message = message;
        this.code = code;
        return this;
    }
    public LiteConfigSlideResult error(String message) {
        this.message = message;
        this.code = CommonConstant.SC_INTERNAL_SERVER_ERROR_500;
        return this;
    }
}

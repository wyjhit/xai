package cn.ydxiaoshuai.common.api.vo.api;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author 小帅丶
 * @className WSPalmBean
 * @Description 微算手相
 * @Date 2020/5/8-15:53
 **/
@NoArgsConstructor
@Data
public class WSPalmBean {
    private String desc;
    private int error;
    private int timestamp;
    private DataBean data;

    @NoArgsConstructor
    @Data
    public static class DataBean {

        private String hand_token;
        private String hand_img;
        private LabelsBean labels;
        private String finger1_h;
        private String finger2_h;
        private String finger3_h;
        private String finger4_h;
        private String finger5_h;

        @NoArgsConstructor
        @Data
        public static class LabelsBean {
            private List<List<Integer>> keypoints;
            private List<List<List<Integer>>> label1;
            private List<List<List<Integer>>> label3;
            private List<List<List<Integer>>> label5;
            private List<List<List<Integer>>> label7;
            private List<?> label2;
            private List<?> label4;
            private List<?> label6;
            private List<?> label8;
        }
    }
}

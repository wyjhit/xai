package cn.ydxiaoshuai.common.api.vo.palm;


import lombok.Data;

/**
 * @Description 图片上传返回的内容
 * @author 小帅丶
 * @className PalmImagesBean
 * @Date 2020/1/3-10:41
 **/
@Data
public class PalmImagesBean {

    /**
     * url : https://uploadzxcs.ggwan.com/image/dashuju-oss/4a91e416e7cbbe-444x444.jpg
     * thumb_url :
     * res_id : cd9f74a91e416e7cbbeacb911f1bc239
     */
    /** 图片地址 */
    private String url;
    /** 图片临时地址 */
    private String thumb_url;
    /** 图片标识ID */
    private String res_id;

}

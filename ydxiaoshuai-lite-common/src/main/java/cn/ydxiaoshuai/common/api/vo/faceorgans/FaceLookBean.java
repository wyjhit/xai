package cn.ydxiaoshuai.common.api.vo.faceorgans;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 小帅丶
 * @className FaceLookBean
 * @Description 面相分析
 * @Date 2020/9/18-14:38
 **/
@NoArgsConstructor
@Data
public class FaceLookBean {

    /**
     * extData : {"template_name":"atom/summary","template_id":"G235_1"}
     * commonData : {"feRoot":"https://mms-static.cdn.bcebos.com/graph/graphfe/static","os":"ios","sidList":"10010_10125_10028_10003_10005_10104_10201_10040_10072_10062_10080_10190_10290_10390_10692_10704_10705_10301_10709_10802_10902_11006_10905_10911_11001","graphEnv":"wise","nativeSdk":false,"sf":0,"isNaView":0,"isHalfWap":0}
     * tplData : {"title":"你的面相评分：93分","titleMark":"吉","markType":3,"description":"头脑灵活，反应灵敏。个性爽朗活泼，不计较个人得失。懂得上进，很容易获得合作者的敬仰。做起事来寸土必争，能力更胜男士一筹。平时高雅端庄，非常爱美，爱干净。","moreText":"查看更多面相分析","hasMore":true,"titleUrl":"https://graph.baidu.com/view/facereadresult?sign=221163b7e1cf86e0cfcf601600411174&srcp=&tn=wise&idctag=tc&sids=10010_10125_10028_10003_10005_10104_10201_10040_10072_10062_10080_10190_10290_10390_10692_10704_10705_10301_10709_10802_10902_11006_10905_10911_11001&logid=2121008205&entrance=","moreUrl":"https://graph.baidu.com/view/facereadresult?sign=221163b7e1cf86e0cfcf601600411174&srcp=&tn=wise&idctag=tc&sids=10010_10125_10028_10003_10005_10104_10201_10040_10072_10062_10080_10190_10290_10390_10692_10704_10705_10301_10709_10802_10902_11006_10905_10911_11001&logid=2121008205&entrance=","defaultHide":false,"cardType":"face_new"}
     */

    private ExtDataBean extData;
    private CommonDataBean commonData;
    private TplDataBean tplData;

    @NoArgsConstructor
    @Data
    public static class ExtDataBean {
        /**
         * template_name : atom/summary
         * template_id : G235_1
         */

        private String template_name;
        private String template_id;
    }

    @NoArgsConstructor
    @Data
    public static class CommonDataBean {
        /**
         * feRoot : https://mms-static.cdn.bcebos.com/graph/graphfe/static
         * os : ios
         * sidList : 10010_10125_10028_10003_10005_10104_10201_10040_10072_10062_10080_10190_10290_10390_10692_10704_10705_10301_10709_10802_10902_11006_10905_10911_11001
         * graphEnv : wise
         * nativeSdk : false
         * sf : 0
         * isNaView : 0
         * isHalfWap : 0
         */

        private String feRoot;
        private String os;
        private String sidList;
        private String graphEnv;
        private boolean nativeSdk;
        private int sf;
        private int isNaView;
        private int isHalfWap;
    }

    @NoArgsConstructor
    @Data
    public static class TplDataBean {
        /**
         * title : 你的面相评分：93分
         * titleMark : 吉
         * markType : 3
         * description : 头脑灵活，反应灵敏。个性爽朗活泼，不计较个人得失。懂得上进，很容易获得合作者的敬仰。做起事来寸土必争，能力更胜男士一筹。平时高雅端庄，非常爱美，爱干净。
         * moreText : 查看更多面相分析
         * hasMore : true
         * titleUrl : https://graph.baidu.com/view/facereadresult?sign=221163b7e1cf86e0cfcf601600411174&srcp=&tn=wise&idctag=tc&sids=10010_10125_10028_10003_10005_10104_10201_10040_10072_10062_10080_10190_10290_10390_10692_10704_10705_10301_10709_10802_10902_11006_10905_10911_11001&logid=2121008205&entrance=
         * moreUrl : https://graph.baidu.com/view/facereadresult?sign=221163b7e1cf86e0cfcf601600411174&srcp=&tn=wise&idctag=tc&sids=10010_10125_10028_10003_10005_10104_10201_10040_10072_10062_10080_10190_10290_10390_10692_10704_10705_10301_10709_10802_10902_11006_10905_10911_11001&logid=2121008205&entrance=
         * defaultHide : false
         * cardType : face_new
         */

        private String title;
        private String titleMark;
        private int markType;
        private String description;
        private String moreText;
        private boolean hasMore;
        private String titleUrl;
        private String moreUrl;
        private boolean defaultHide;
        private String cardType;
    }
}

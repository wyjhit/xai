package cn.ydxiaoshuai.common.constant;

/**
 * @author 小帅丶
 * @className FaceConts
 * @Description 枚举类
 * @Date 2020/5/29-16:01
 **/
public enum  FaceConts {
    BASE64,URL,FACE_TOKEN
}

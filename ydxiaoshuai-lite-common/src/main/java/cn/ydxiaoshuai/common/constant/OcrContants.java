package cn.ydxiaoshuai.common.constant;

/**
 * @author 小帅丶
 * @className OcrContants
 * @Description 文字识别常量
 * @Date 2020/9/24-16:08
 **/
public class OcrContants {
    public static String IMAGE_STATUS_NORMAL = "normal";
    public static String IMAGE_STATUS_REVERSED_SIDE = "reversed_side";
    public static String IMAGE_STATUS_BLURRED = "blurred";
    public static String IMAGE_STATUS_OTHER_TYPE_CARD = "other_type_card";
    public static String IMAGE_STATUS_OVER_EXPOSURE = "over_exposure";
    public static String IMAGE_STATUS_UNKNOWN = "unknown";
    public static String IMAGE_STATUS_NON_IDCARD = "non_idcard";

    public static String RISK_TYPE_NORMAL = "normal";
    public static String RISK_TYPE_COPY = "copy";
    public static String RISK_TYPE_SCREEN = "screen";
    public static String RISK_TYPE_UNKNOWN = "unknown";
    public static String RISK_TYPE_TEMPORARY = "temporary";

    public static String BANK_CARD_TYPE_0 = "0";
    public static String BANK_CARD_TYPE_1 = "1";
    public static String BANK_CARD_TYPE_2 = "2";
}

package cn.ydxiaoshuai.common.system.vo;

import cn.ydxiaoshuai.common.api.vo.BaseResponseBean;
import cn.ydxiaoshuai.common.api.vo.oss.TencentPutObjectResult;
import cn.ydxiaoshuai.common.constant.CommonConstant;
import lombok.Data;

/**
 * @author 小帅丶
 * @className UploadOssResponseBean
 * @Description 上传返回的bean
 * @Date 2020/9/4-14:48
 **/
@Data
public class UploadOssResponseBean extends BaseResponseBean {

    private TencentPutObjectResult data;

    public UploadOssResponseBean success(String message, String message_zh,TencentPutObjectResult data) {
        this.message = message;
        this.message_zh = message_zh;
        this.code = CommonConstant.SC_OK_200;
        this.data = data;
        return this;
    }

    public UploadOssResponseBean fail(String message, String message_zh, Integer code) {
        this.message = message;
        this.message_zh = message_zh;
        this.code = code;
        return this;
    }

    public UploadOssResponseBean error(String message, String message_zh) {
        this.message = message;
        this.message_zh = message_zh;
        this.code = CommonConstant.SC_INTERNAL_SERVER_ERROR_500;
        return this;
    }
}

package cn.ydxiaoshuai.modules.entity;

import java.io.Serializable;
import java.util.Date;

import cn.ydxiaoshuai.common.aspect.annotation.Dict;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;

/**
 * @Description: API日志记录表
 * @Author: 小帅丶
 * @Date:   2020-04-30
 * @Version: V1.0
 */
@Data
@TableName("lite_api_log")
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="lite_api_log对象", description="API日志记录表")
public class LiteApiLog {

	/**主键id*/
	@TableId(type = IdType.ID_WORKER_STR)
    @ApiModelProperty(value = "主键id")
	private String id;
	/**客户端类型*/
	@Excel(name = "客户端类型", width = 15)
	@ApiModelProperty(value = "客户端类型")
	private String clientName;
	/**日志ID*/
	@Excel(name = "日志ID", width = 15)
    @ApiModelProperty(value = "日志ID")
	private String logId;
	/**请求的IP*/
	@Excel(name = "请求的IP", width = 15)
    @ApiModelProperty(value = "请求的IP")
	private String requestIp;
	/**请求的URL*/
	@Excel(name = "请求的URL", width = 15)
    @ApiModelProperty(value = "请求的URL")
	private String requestUrl;
	/**请求的参数*/
	@Excel(name = "请求的参数", width = 15)
    @ApiModelProperty(value = "请求的参数")
	private Object requestParam;
	/**接口返回的内容*/
	@Excel(name = "接口返回的内容", width = 15)
    @ApiModelProperty(value = "接口返回的内容")
	private Object responseText;
	/**日志接口类型*/
	@Excel(name = "日志接口类型", width = 15)
    @ApiModelProperty(value = "日志接口类型")
	@Dict(dicCode = "api_type")
	private Integer logType;
	/**耗时(ms)*/
	@Excel(name = "耗时(ms)", width = 15)
    @ApiModelProperty(value = "耗时(ms)")
	private String timeConsuming;
	/**异常信息*/
	@Excel(name = "异常信息", width = 15)
    @ApiModelProperty(value = "异常信息")
	private Object errorMsg;
	/**创建人*/
	@Excel(name = "访问的用户", width = 15)
	@ApiModelProperty(value = "访问的用户")
	private String userId;
	/**创建人*/
	@Excel(name = "创建人", width = 15)
    @ApiModelProperty(value = "创建人")
	private String createBy;
	/**创建时间*/
	@Excel(name = "创建时间", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建时间")
	private Date createTime;
}

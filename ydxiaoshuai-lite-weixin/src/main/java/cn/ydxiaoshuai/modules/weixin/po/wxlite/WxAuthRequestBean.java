package cn.ydxiaoshuai.modules.weixin.po.wxlite;

import lombok.Data;

/**
 * @author 小帅丶
 * @className WxAuthRequestBean
 * @Description 微信授权
 * @Date 2019/8/5-17:50
 **/
@Data
public class WxAuthRequestBean {
    private String type;
    private long timeStamp;
    private Target target;
    private Target currentTarget;
    private Detail detail;
    @Data
    public static class Target{
        private String id;
        private int offsetLeft;
        private int offsetTop;
        private DataSet dataset;
    }
    @Data
    public static class Detail{
        private String errMsg;
        private String rawData;
        private String signature;
        private String encryptedData;
        private String iv;
        private UserInfo userInfo;

    }
    @Data
    public static class UserInfo{
        private String nickName;
        private String gender;
        private String language;
        private String city;
        private String province;
        private String country;
        private String avatarUrl;
        //解密数据返回
        private String openid;
    }
    @Data
    public static class DataSet{

    }
}

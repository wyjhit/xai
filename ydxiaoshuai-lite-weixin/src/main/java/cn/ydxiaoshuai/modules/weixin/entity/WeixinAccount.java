package cn.ydxiaoshuai.modules.weixin.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;

/**
 * @Description: 微信公众账号表
 * @Author: 小帅丶
 * @Date:   2020-09-10
 * @Version: V1.0
 */
@Data
@TableName("weixin_account")
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="weixin_account对象", description="微信公众账号表")
public class WeixinAccount {
    
	/**主键*/
	@TableId(type = IdType.ID_WORKER_STR)
    @ApiModelProperty(value = "主键")
	private String id;
	/**微信公众号名称*/
	@Excel(name = "微信公众号名称", width = 15)
    @ApiModelProperty(value = "微信公众号名称")
	private String accountName;
	/**0服务号 1订阅号 2小程序 3企业号*/
	@Excel(name = "0服务号 1订阅号 2小程序 3企业号", width = 15)
    @ApiModelProperty(value = "0服务号 1订阅号 2小程序 3企业号")
	private Integer accountType;
	/**微信APPID*/
	@Excel(name = "微信APPID", width = 15)
    @ApiModelProperty(value = "微信APPID")
	private String accountAppid;
	/**微信APPSECRET*/
	@Excel(name = "微信APPSECRET", width = 15)
    @ApiModelProperty(value = "微信APPSECRET")
	private String accountAppsecret;
	/**微信原始ID*/
	@Excel(name = "微信原始ID", width = 15)
    @ApiModelProperty(value = "微信原始ID")
	private String accountWxid;
	/**内部编号*/
	@Excel(name = "内部编号", width = 15)
    @ApiModelProperty(value = "内部编号")
	private String accountCode;
	/**是否启用*/
	@Excel(name = "是否启用", width = 15)
	@ApiModelProperty(value = "是否启用")
	private Integer isEnable;
	/**创建人*/
	@Excel(name = "创建人", width = 15)
    @ApiModelProperty(value = "创建人")
	private String createBy;
	/**创建时间*/
	@Excel(name = "创建时间", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建时间")
	private Date createTime;
	/**更新人*/
	@Excel(name = "更新人", width = 15)
    @ApiModelProperty(value = "更新人")
	private String updateBy;
	/**更新时间*/
	@Excel(name = "更新时间", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新时间")
	private Date updateTime;
}

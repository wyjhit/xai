package cn.ydxiaoshuai.modules.system.service;

import cn.ydxiaoshuai.modules.system.entity.SysDepartRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 部门角色
 * @Author: 小帅丶
 * @Date:   2020-02-12
 * @Version: V1.0
 */
public interface ISysDepartRoleService extends IService<SysDepartRole> {

}
